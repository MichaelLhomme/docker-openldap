#!/bin/bash
source .env

echo "This command should fail after authentication with 'Inappropriate authentication (48) additional info: anonymous bind disallowed'..."
docker-compose exec -e LDAPTLS_REQCERT=never openldap ldapsearch -x -H ldap://openldap:1389 -b $LDAP_ROOT

if [ "$?" != "48" ] ; then
  echo "!!! Error, LDAP server does not enforce authentication"
  exit 1
fi
