#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# TODO fix this
# openldap    | 62260570.07415f42 0x7ff26e1344c0 olcPasswordHash: value #0: setting password scheme in the global entry is deprecated. The server may refuse to start if it is provided by a loadable module, please move it to the frontend database instead

info "Configure internal password hash TO SSHA"
ldapmodify -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: cn=config
changetype: modify
add: olcPasswordHash
olcPasswordHash: {SSHA}
EOF
