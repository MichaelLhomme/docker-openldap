#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

SYSTEM_DN=cn=system,$LDAP_ROOT
ADMINISTRATOR_DN=uid=administrator,$LDAP_ROOT

# System user used by services (such as sssd) for read only operations
info "Create system user for readonly operations"
ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
dn: $SYSTEM_DN
cn: system
objectClass: simpleSecurityObject
objectClass: organizationalRole
userPassword: $SYSUSER_PASSWORD
EOF

# Main administrator user (DO NOT use cn=admin for tasks other than system maintenance)
info "Create administrator user"
ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
dn: $ADMINISTRATOR_DN
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
uid: administrator
cn: Administrator
sn: Administrator
givenName: Administrator
userPassword: $LDAP_ADMIN_PASSWORD
description: LDAP Administrator
EOF

# Default groups
# - ldapadmins : internal usage for ACLs
# - ldapusers : default POSIX group for every POSIX account
info "Create ldapusers group for all users"
ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
dn: cn=ldapadmins,ou=groups,$LDAP_ROOT
objectClass: groupOfNames
cn: ldapadmins
member: $ADMINISTRATOR_DN

dn: cn=ldapmanagers,ou=groups,$LDAP_ROOT
objectClass: groupOfNames
cn: ldapmanagers
member: $ADMINISTRATOR_DN

dn: cn=ldapusers,ou=groups,$LDAP_ROOT
objectClass: posixGroup
objectClass: groupOfNames
cn: ldapusers
gidNumber: 2000
member: $SYSTEM_DN
EOF
