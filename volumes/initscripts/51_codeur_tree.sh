#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# Initial tree structure
info "Creating initial tree structure"

ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
# Root OU node for teachers
dn: ou=teachers,ou=users,$LDAP_ROOT
objectClass: organizationalUnit
ou: teachers
description: Root OU node for teachers

# Root OU node for students
dn: ou=students,ou=users,$LDAP_ROOT
objectClass: organizationalUnit
ou: students
description: Root OU node for students
EOF
