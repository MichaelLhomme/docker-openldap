#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# Extra schema for SSH keys
# https://www.ossramblings.com/using-ldap-to-store-ssh-public-keys-with-sssd
# https://fy.blackhats.net.au/blog/html/2015/07/10/SSH_keys_in_ldap.html
info "Creating custom schema for SSH keys"

ldapadd -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: cn=openssh-openldap,cn=schema,cn=config
objectClass: olcSchemaConfig
cn: openssh-openldap
olcAttributeTypes: {0}( 1.3.6.1.4.1.24552.500.1.1.1.13 NAME 'sshPublicKey'
  DESC 'MANDATORY: OpenSSH Public key'
  EQUALITY octetStringMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.40 )
olcObjectClasses: {0}( 1.3.6.1.4.1.24552.500.1.1.2.0 NAME 'ldapPublicKey'
  DESC 'MANDATORY: OpenSSH LPK objectclass'
  SUP top AUXILIARY 
  MAY ( sshPublicKey $ uid ) )
EOF
